<?php include 'includes/header.php' ?>



    <!-- Main Content -->

    <div id="main-content" class="site-main clearfix">

        <div id="content-wrap">

            <div id="site-content" class="site-content clearfix">

                <div id="inner-content" class="inner-content-wrap">

                    <div class="page-content">


                        
						<div id="featured-title" class="clearfix featured-title-left">
						        <div id="featured-title-inner" class="wprt-container clearfix">
						            <div class="featured-title-inner-wrap">
						                <div class="featured-title-heading-wrap">
						                    <h1 class="featured-title-heading ">A EMPRESA</h1>
						                </div>
						                
						                
						            </div>
						        </div>
						    </div>
						    
						    
						    
						    
						    <div id="main-content" class="site-main clearfix">
        <div id="content-wrap">
            <div id="site-content" class="site-content clearfix">
                <div id="inner-content" class="inner-content-wrap">
                    <div class="page-content">
                        <!-- About 2 -->
                        <div class="row-about-2">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="wprt-spacer clearfix" data-desktop="80" data-mobi="60" data-smobi="40"></div>
                                    </div>
                                        
                                    <div class="col-md-6">
                                        <div class="wprt-headings clearfix">
                                            <h2 class="heading">NOSSA HISTÓRIA</h2>
                                            <div class="sep"></div>
                                        </div><!-- /.wprt-headings -->

                                        <div class="wprt-spacer clearfix" data-desktop="30" data-mobi="25" data-smobi="25"></div>

                                       <p>A UNIFORBET é uma empresa localizada em Betim-MG que atua no mercado produzindo <a href="uniformes"/>uniformes </a>
											profissionais com um maquinário moderno e mão de obra 
											especializada, garantindo ao <a href="clientes"/>cliente</a> satisfação, qualidade e pontualidade.<br>
											<br>Fundada na cidade de Betim, atuando desde 2016 no mercado, a empresa iniciou suas atividades fabricando 
											<a href="uniformes"/>uniformes</a> para pequenas oficinas mecânicas, lanchonetes, farmácias e padarias. 
											Tudo isso marcou presença na história da confecção, colaborando para o seu 
											crescimento e consolidando a sua qualidade no mercado.<br><br>Com grande 
											experiência no mercado adquirida ao longo do tempo, a empresa sempre buscou 
											investir em inovações, conciliando agilidade e qualidade em seus produtos, e 
											atualmente atende a metalúrgicas, construtoras, concessionárias, indústrias e 
											transportadoras, entre outras que necessitam uniformização dos setores 
											operacionais e administrativos.</p>
											
                                        <div class="wprt-spacer clearfix" data-desktop="13" data-mobi="13" data-smobi="13"></div>

                                        <div class="wprt-divider divider-center divider-solid"></div>

                                        

                                        <div class="wprt-spacer clearfix" data-desktop="0" data-mobi="40" data-smobi="40"></div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="wprt-image-slider arrow-style-2 wd-40 arrow-square arrow-center offset0i offset-v0 has-arrows" data-auto="false" data-loop="false" data-gap="0" data-column="1" data-column2="1" data-column3="1">
                                            <div class="owl-carousel owl-theme">
                                          	  <div class="item">
                                                    <img src="assets/img/abouts/about-3.jpg" alt="Image">             
                                                    
                                                    <a class="zoom-popup cbp-lightbox" href="assets/img/abouts/about-3-1366-999.jpg" data-title="">
                                                        <i class="fa fa-arrows"></i>
                                                    </a>
                                                </div>
                                                <div class="item">
                                                    <img src="assets/img/abouts/about-1.jpg" alt="Image">             
                                                    
                                                    <a class="zoom-popup cbp-lightbox" href="assets/img/abouts/about-1-1366-900.jpg" data-title="">
                                                        <i class="fa fa-arrows"></i>
                                                    </a>
                                                </div>

                                                <div class="item">
                                                    <img src="assets/img/abouts/about-2.jpg" alt="Image">             
                                                    
                                                    <a class="zoom-popup cbp-lightbox" href="assets/img/abouts/about-2-1366-999.jpg" data-title="">
                                                        <i class="fa fa-arrows"></i>
                                                    </a>
                                                </div>

                                                 
                                            </div>
                                        </div><!-- /.wprt-image-slider -->
                                    </div>

                                    <div class="col-md-12">
                                        <div class="wprt-spacer clearfix" data-desktop="80" data-mobi="60" data-smobi="40"></div>
                                    </div>
                                </div><!-- /.row -->
                            </div><!-- /.container -->
                        </div>


                    </div><!-- /.page-content -->
                </div><!-- /#inner-content -->
            </div><!-- /#site-content -->
        </div><!-- /#content-wrap -->
    </div>



                        <!-- Testimonials 1 -->

                        

                        <!-- /Testimonials 1 -->

                    </div><!-- /.page-content -->

                </div><!-- /#inner-content -->

            </div><!-- /#site-content -->

        </div><!-- /#content-wrap -->

    </div>

    <!-- /Main Content -->



    <?php include 'includes/footer.php' ?>