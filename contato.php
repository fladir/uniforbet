<?php include 'includes/header.php' ?>



<!-- Main Content -->

<div id="main-content" class="site-main clearfix">

    <div id="content-wrap">

        <div id="site-content" class="site-content clearfix">

            <div id="inner-content" class="inner-content-wrap">

                <div class="page-content">


                    <div id="featured-title" class="clearfix featured-title-left">
                        <div id="featured-title-inner" class="wprt-container clearfix">
                            <div class="featured-title-inner-wrap">
                                <div class="featured-title-heading-wrap">
                                    <h1 class="featured-title-heading ">CONTATO</h1>
                                </div>


                            </div>
                        </div>
                    </div>


                    <div id="main-content" class="site-main clearfix">
                        <div id="content-wrap">
                            <div id="site-content" class="site-content clearfix">
                                <div id="inner-content" class="inner-content-wrap">
                                    <div class="page-content">
                                        <!-- Icon Information -->
                                        <div class="row-about-2">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="wprt-spacer clearfix" data-desktop="80" data-mobi="60" data-smobi="40"></div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="wprt-content-box clearfix " data-margin="" data-mobimargin="">
                                                            <div class="inner">
                                                                <div class="wprt-icon-box style-3 font-70 clearfix icon-top align-center">
                                                                    <div class="icon-wrap"><img alt="" src="assets/img/cont3.png" style="height: 52px" width="52"></span></div>
                                                                    <h3 class="heading">
                                                                        Telefones
                                                                    </h3>
                                                                    <div><span>
                                                                            <a target="_blank" href="tel:3125715995" />
                                                                            31 2571-5995
                                                                            </a>
                                                                            /
                                                                            <a target="_blank" href="tel:3125715997" />
                                                                            2571-5997
                                                                            </a>
                                                                        </span></div>
                                                                    <div><span>
                                                                            <a target="_blank" href="tel:31996207696" />
                                                                            31 99620-7696
                                                                            </a>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div><!-- /.wprt-content-box -->

                                                        <div class="wprt-spacer clearfix" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                                    </div><!-- /.col-md-4 -->

                                                    <div class="col-md-4">
                                                        <div class="wprt-content-box clearfix " data-margin="" data-mobimargin="">
                                                            <div class="inner">
                                                                <div class="wprt-icon-box style-3 font-50 clearfix icon-top align-center">
                                                                    <div class="icon-wrap">
                                                                        <img alt="" src="assets/img/cont2.png" style="height: 52px" width="52"></span>
                                                                    </div>
                                                                    <h3 class="heading">
                                                                        Endereço
                                                                    </h3>
                                                                    <a target="_blank" href="https://www.google.com/maps/place/UNIFORBET+UNIFORMES+PROFISSIONAIS/@-19.9682689,-44.1920461,15z/data=!4m2!3m1!1s0x0:0xb927590db136aa2c?sa=X&ved=2ahUKEwiUlc_L26ruAhUUDrkGHeKoDO8Q_BIwCnoECBcQBQ">
                                                                        <div><span>Av Das Américas, 354 - B. Filadélfia</span></div>
                                                                        <div><span>Betim - MG</span></div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div><!-- /.wprt-content-box -->

                                                        <div class="wprt-spacer clearfix" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                                    </div><!-- /.col-md-4 -->

                                                    <div class="col-md-4">
                                                        <div class="wprt-content-box clearfix " data-margin="" data-mobimargin="">
                                                            <div class="inner">
                                                                <div class="wprt-icon-box style-3 font-60 clearfix icon-top align-center">
                                                                    <div class="icon-wrap">
                                                                        <img alt="" src="assets/img/cont1.png" style="height: 52px" width="52"></span>
                                                                    </div>
                                                                    <h3 class="heading">
                                                                        E-mail
                                                                    </h3>
                                                                    <div>
                                                                        <a href="mailto:uniforbet@gmail.com" target="_blank">
                                                                            <span>uniforbet@gmail.com</span>
                                                                        </a>
                                                                    </div>
                                                                    <div></div>
                                                                </div>
                                                            </div>
                                                        </div><!-- /.wprt-content-box -->
                                                    </div><!-- /.col-md-4 -->

                                                    <div class="col-md-12">
                                                        <div class="wprt-spacer clearfix" data-desktop="80" data-mobi="60" data-smobi="40"></div>
                                                    </div>
                                                </div><!-- /.Row -->
                                            </div><!-- /.container -->
                                        </div>
                                        <!-- /Icon Information -->

                                        <!-- Google Map -->

                                        <!-- /Google Map -->

                                        <!-- Leave a Message -->
                                        <div class="row-about-2">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-10">
                                                        <div class="wprt-spacer clearfix" data-desktop="74" data-mobi="60" data-smobi="40"></div>

                                                        <div class="wprt-headings clearfix text-center style-3">
                                                            <h2 class="heading">FALE CONOSCO</h2>
                                                            <div style="padding-bottom: 18px">O formulário abaixo serve para coletar as principais informações para que possamos fazer um orçamento sem compromisso.

                                                                <br>Preencha as informações abaixo corretamente, quanto mais informações forem fornecidas mais<br>&nbsp;próximo do valor real ficará o orçamento de seu projeto.
                                                            </div>
                                                            <div class="sep"></div>
                                                        </div><!-- /.wprt-headings -->

                                                        <div class="wprt-spacer clearfix" data-desktop="50" data-mobi="40" data-smobi="40"></div>


                                                        <div class="wprt-content-box style-2">
                                                            <form action="#" method="post" class="contact-form wpcf7-form">
                                                                <div class="wprt-contact-form-2">
                                                                    <span class="wpcf7-form-control-wrap name">
                                                                        <input type="text" tabindex="1" id="name" name="name" value="" class="wpcf7-form-control" placeholder="Nome" required>
                                                                    </span>
                                                                    <span class="wpcf7-form-control-wrap email">
                                                                        <input type="email" tabindex="2" id="email" name="email" value="" class="wpcf7-form-control" placeholder="E-mail" required>
                                                                    </span>
                                                                    <span class="wpcf7-form-control-wrap phone">
                                                                        <input type="text" tabindex="3" id="phone" name="phone" value="" class="wpcf7-form-control" placeholder="Telefone">
                                                                    </span>
                                                                    <span class="wpcf7-form-control-wrap subject">
                                                                        <input type="text" tabindex="4" id="subject" name="subject" value="" class="wpcf7-form-control" placeholder="Assunto" required>
                                                                    </span>
                                                                    <span class="wpcf7-form-control-wrap message">
                                                                        <textarea name="message" tabindex="5" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" placeholder="Mensagem *" required></textarea>
                                                                    </span>
                                                                    <div class="wrap-submit">
                                                                        <input type="submit" value="ENVIAR MENSAGEM" class="submit wpcf7-form-control wpcf7-submit" id="submit" name="submit">
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div><!-- /.wprt-content-box -->

                                                        <div class="wprt-spacer clearfix" data-desktop="80" data-mobi="60" data-smobi="40"></div>

                                                    </div>

                                                    <div class="col-md-1"></div>
                                                </div><!-- /.Row -->
                                            </div><!-- /.container -->
                                        </div>

                                        <!-- /Leave a Message -->


                                    </div><!-- /.page-content -->
                                </div><!-- /#inner-content -->
                            </div><!-- /#site-content -->
                        </div><!-- /#content-wrap -->
                    </div>

                    <!-- Testimonials 1 -->



                    <!-- /Testimonials 1 -->

                </div><!-- /.page-content -->

            </div><!-- /#inner-content -->

        </div><!-- /#site-content -->

    </div><!-- /#content-wrap -->

</div>

<!-- /Main Content -->



<?php include 'includes/footer.php' ?>