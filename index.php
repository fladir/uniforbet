<!DOCTYPE html>

<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<!--<![endif]-->


<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-QN3D2XWC7S"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-QN3D2XWC7S');
    </script>

    <!-- Basic Page Needs -->

    <meta charset="utf-8">

    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <title>Uniforbet - Uniformes Profissionais e Camisetas em Betim</title>

    <meta name="description" content="O melhor em uniformes em Betim. Alta Tecnologia, Agilidade na Entrega e Altíssima Precisão em Todos Detalhes. Comodidades: Atendimento Direto, Agilidade na Entrega, Redução de Custos">

    <meta name="keywords" content="uniformes em betim, camisas em betim, camisetas em betim, bordados em betim, uniforme administrativo em betim, uniforme operacional em betim, tecidos em betim, camisas, camisetas, bordados, uniforme administrativo, uniforme operacional, tecidos">





    <!-- Mobile Specific Metas -->

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />

    <!-- Theme Style -->

    <link rel="stylesheet" type="text/css" href="style.min.css">



    <!-- Favicon and touch icons  -->

    <link rel="shortcut icon" href="assets/icon/favicon.png">

    <link rel="apple-touch-icon-precomposed" href="assets/icon/apple-touch-icon-158-precomposed.png">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

</head>



<body class="front-page no-sidebar menu-has-search header-style-1">



    <div id="wrapper" class="animsition">

        <div id="page" class="clearfix">

            <!-- Header Wrap -->

            <div id="site-header-wrap">

                <!-- Top Bar -->

                <div id="top-bar">

                    <div id="top-bar-inner" class="wprt-container">

                        <div class="top-bar-inner-wrap">

                            <div class="top-bar-content style-1">

                                <span class=" content">
                                    <img alt="" src="assets/img/tel.png" style="margin-right: 10px" /> <a target="_blank" href="tel:3125715995" />31 2571-5995</a> / <a target="_blank" href="tel:3125715997" />2571-5997</a></span>

                                <span class="content"><img alt="" src="assets/img/local.png" style="margin-right: 10px"> <a target="_blank" href="https://www.google.com/maps/place/UNIFORBET+UNIFORMES+PROFISSIONAIS/@-19.9682689,-44.1920461,15z/data=!4m2!3m1!1s0x0:0xb927590db136aa2c?sa=X&ved=2ahUKEwiUlc_L26ruAhUUDrkGHeKoDO8Q_BIwCnoECBcQBQ">Av Das Américas, 354 - B. Filadélfia</a></span>

                            </div><!-- /.top-bar-content -->



                            <div class="top-bar-socials style-1">

                                <div class="inner">

                                    <span class="icons">


                                        <a href="https://www.facebook.com/uniforbet/" target="_blank" title="Facebook">

                                            <span class="fa" aria-hidden="true"><img alt="" src="assets/img/face.png"></span>

                                        </a>



                                        <a href="https://www.instagram.com/uniforbet/" target="_blank" title="Instagram"><span class="fa" aria-hidden="true"><img alt="" src="assets/img/insta.png"></span></a></span>
                                </div><!-- /.top-bar-socials -->

                            </div>

                        </div>

                    </div>
                </div>

                    <!-- /Top Bar -->



                    <!-- Header -->

                    <header id="site-header">

                        <div id="site-header-inner" class="wprt-container">

                            <div class="site-header-wrap-inner">

                                <div id="site-logo" class="clearfix">

                                    <div id="site-logo-inner">

                                        <a href="/" title="Uniforbet" rel="home" class="main-logo">

                                            <img src="assets/img/logo.png" width="210" height="42" alt="Contrucst" data-retina="assets/img/logo.png" data-width="210" data-height="42">

                                        </a>

                                    </div>

                                </div><!-- /#site-logo -->



                                <div class="mobile-button"><span></span></div><!-- /mobile menu button -->



                                <nav id="main-nav" class="main-nav">

                                    <ul>

                                        <li class="current-menu-item"><a href="/">Home</a></li>

                                        <li><a href="empresa">A empresa</a></li>

                                        <li><a href="uniformes">Uniformes</a></li>

                                        <li><a href="clientes">Clientes</a></li>

                                        <li><a href="tecidos">Tecidos</a></li>

                                        <li><a href="bordados">Bordados</a></li>

                                        <li><a href="orcamento">Orçamento</a></li>

                                        <li><a href="contato">Contato</a></li>

                                    </ul>

                                </nav><!-- /#main-nav -->



                                <!-- /#header-search -->

                            </div>

                        </div>

                    </header>

                    <!-- /Header -->

                </div>

                <!-- /Header Wrap -->



                <!-- Main Content -->

                <div id="main-content" class="site-main clearfix">

                    <div id="content-wrap">

                        <div id="site-content" class="site-content clearfix">

                            <div id="inner-content" class="inner-content-wrap">

                                <div class="page-content">

                                    <!-- Slider -->

                                    <div class="rev_slider_wrapper fullwidthbanner-container">

                                        <div id="rev-slider1" class="rev_slider fullwidthabanner">

                                            <ul>

                                                <!-- Slide 1 -->

                                                <li data-transition="random">

                                                    <!-- Main Image -->

                                                    <img src="assets/img/slider/slide-3.jpg" alt="" data-bgposition="center center" data-no-retina>



                                                    <!-- Layers -->

                                                    <div class="tp-caption tp-resizeme text-white font-heading" data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']" data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-100','-100']" data-fontsize="['48','48','48','40']" data-lineheight="['58','58','58','50']" data-width="full" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="700" data-splitin="none" data-splitout="none" data-responsive_offset="on">

                                                        CONHEÇA<br>NOSSA LINHA INDUSTRIAL

                                                    </div>







                                                    <div class="tp-caption" data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']" data-y="['middle','middle','middle','middle']" data-voffset="['12','120','120','140']" data-width="full" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on">

                                                        <a href="uniformes" class="wprt-button big">CLIQUE AQUI</a>

                                                    </div>

                                                </li>

                                                <!-- /End Slide 1 -->



                                                <!-- Slide 2 -->

                                                <li data-transition="random">

                                                    <!-- Main Image -->

                                                    <img src="assets/img/slider/slide-2.jpg" alt="" data-bgposition="center center" data-no-retina>



                                                    <!-- Layers -->

                                                    <div class="tp-caption tp-resizeme text-white font-heading" data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']" data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-100','-100']" data-fontsize="['48','48','48','40']" data-lineheight="['58','58','58','50']" data-width="full" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="700" data-splitin="none" data-splitout="none" data-responsive_offset="on">

                                                        EXCELÊNCIA EM<br>UNIFORMES PROFISSIONAIS

                                                    </div>

                                                </li>


                                                <li data-transition="random">

                                                    <!-- Main Image -->

                                                    <img src="assets/img/slider/slide1.jpg" alt="" data-bgposition="center center" data-no-retina>



                                                    <!-- Layers -->

                                                    <div class="tp-caption tp-resizeme text-white font-heading" data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']" data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-100','-100']" data-fontsize="['48','48','48','40']" data-lineheight="['58','58','58','50']" data-width="full" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="700" data-splitin="none" data-splitout="none" data-responsive_offset="on">

                                                        SOLICITE UM ORÇAMENTO<br>AGORA MESMO

                                                    </div>







                                                    <div class="tp-caption" data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']" data-y="['middle','middle','middle','middle']" data-voffset="['12','120','120','140']" data-width="full" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on">

                                                        <a href="contato" class="wprt-button big">CLIQUE AQUI</a>

                                                    </div>











                                                </li>


                                                <!-- /End Slide 2 -->

                                            </ul>

                                        </div>

                                    </div>

                                    <!-- /Slider -->



                                    <!-- Action Box 1 -->



                                    <!-- /Action Box 1 -->


                                    <div id="main-content" class="site-main clearfix">
                                        <div id="content-wrap">
                                            <div id="site-content" class="site-content clearfix">
                                                <div id="inner-content" class="inner-content-wrap">
                                                    <div class="page-content">
                                                        <!-- Services 1 -->
                                                        <div class="row-services-1">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="wprt-spacer clearfix" data-desktop="80" data-mobi="60" data-smobi="40"></div>

                                                                        <div class="wprt-headings clearfix text-center">

                                                                            <h2 class="heading">Seja bem vindo ao nosso site</h2>

                                                                            <div class="sep"></div>

                                                                            <p class="sub-heading">

                                                                                Nossa linha de produtos está apta a atender aos profissionais de diversos ramos de atividades que necessitam de uniformes: sociais, industriais, hospitalares, chefes de cozinha e equipe, limpeza e conservação, hotelaria, dentre outros, além de confeccionarmos uniformes personalizados para atender a necessidade especifica de cada cliente.

                                                                            </p>

                                                                        </div>

                                                                        <div class="wprt-spacer clearfix" data-desktop="53" data-mobi="30" data-smobi="30"></div>

                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div class="wprt-image-box style-1 clearfix ">
                                                                            <div class="item">
                                                                                <div class="inner">
                                                                                    <div class="thumb">
                                                                                        <img width="370" height="250" src="assets/img/services/service-1.jpg" alt="Image">
                                                                                    </div>

                                                                                    <div class="text-wrap">
                                                                                        <h3 class="title"><a target="_blank" href="#">Quem Somos</a></h3>

                                                                                        <div class="desc">A UNIFORBET é uma empresa que atua no mercado produzindo uniformes profissionais com um maquinário moderno e mão de obra especializada</div>

                                                                                        <div class="wprt-btn"><a target="_blank" class="simple-link font-heading" href="empresa">SAIBA MAIS</a></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div><!-- /.wprt-image-box style-1 -->



                                                                        <!-- /.wprt-image-box style-1 -->

                                                                        <div class="wprt-spacer clearfix" data-desktop="0" data-mobi="40" data-smobi="40"></div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div class="wprt-image-box style-1 clearfix ">
                                                                            <div class="item">
                                                                                <div class="inner">
                                                                                    <div class="thumb">
                                                                                        <img width="370" height="250" src="assets/img/services/service-2.jpg" alt="Image">
                                                                                    </div>

                                                                                    <div class="text-wrap">
                                                                                        <h3 class="title"><a target="_blank" href="#">Uniformes</a></h3>

                                                                                        <div class="desc">Completa linha de uniformes para diversos perfis profissionais. Ampla grade de tamanhos para garantir a perfeita adequação visual.</div>

                                                                                        <div class="wprt-btn"><a target="_blank" class="simple-link font-heading" href="uniformes">SAIBA MAIS</a></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div><!-- /.wprt-image-box style-1 -->



                                                                        <!-- /.wprt-image-box style-1 -->

                                                                        <div class="wprt-spacer clearfix" data-desktop="0" data-mobi="40" data-smobi="40"></div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div class="wprt-image-box style-1 clearfix ">
                                                                            <div class="item">
                                                                                <div class="inner">
                                                                                    <div class="thumb">
                                                                                        <img width="370" height="250" src="assets/img/services/service-3.jpg" alt="Image">
                                                                                    </div>

                                                                                    <div class="text-wrap">
                                                                                        <h3 class="title"><a target="_blank" href="#">Bordados</a> </h3>

                                                                                        <div class="desc">Processo eletrônico onde bordamos sua logomarca em um software específico, de maneira que os fios utilizados formem o desenho desejado.</div>

                                                                                        <div class="wprt-btn"><a target="_blank" class="simple-link font-heading" href="bordados">SAIBA MAIS</a> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div><!-- /.wprt-image-box style-1 -->



                                                                        <!-- /.wprt-image-box style-1 -->
                                                                    </div>

                                                                    <div class="col-md-12">
                                                                        <div class="wprt-spacer clearfix" data-desktop="80" data-mobi="60" data-smobi="40"></div>
                                                                    </div>
                                                                </div>
                                                            </div><!-- /.container -->
                                                        </div>


                                                    </div><!-- /.page-content -->
                                                </div><!-- /#inner-content -->
                                            </div><!-- /#site-content -->
                                        </div><!-- /#content-wrap -->
                                    </div>



                                    <!-- Services 1 -->



                                    <!-- /Services 1 -->



                                    <!-- Portfolio 1-->



                                    <!-- /Portfolio 1-->



                                    <!-- About 1-->



                                    <!-- /About 1 -->



                                    <!-- Video 1 -->



                                    <!-- /Video 1 -->



                                    <!-- News 1 -->



                                    <!-- /News 1 -->



                                    <!-- Testimonials 1 -->



                                    <!-- /Testimonials 1 -->

                                </div><!-- /.page-content -->

                            </div><!-- /#inner-content -->

                        </div><!-- /#site-content -->

                    </div><!-- /#content-wrap -->

                </div>

                <!-- /Main Content -->

                <?php include 'includes/footer.php'; ?>