<!DOCTYPE html>

<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->

<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->


<head>

    <!-- Basic Page Needs -->

    <meta charset="utf-8">

    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <title>Uniforbet - Uniformes Profissionais</title>

    <meta name="description" content="Alta Tecnologia, Agilidade na Entrega e Altíssima Precisão em Todos Detalhes. Comodidades: Atendimento Direto, Agilidade na Entrega, Redução de Custos">

    <meta name="keywords" content="camisas, bordados, uniforme administrativo, uniforme operacional, tecidos">

    



    <!-- Mobile Specific Metas -->

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">



    <!-- Theme Style -->

    <link rel="stylesheet" type="text/css" href="style.css">



    <!-- Favicon and touch icons  -->

    <link rel="shortcut icon" href="assets/icon/favicon.png">

    <link rel="apple-touch-icon-precomposed" href="assets/icon/apple-touch-icon-158-precomposed.png">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

</head>



<body class="front-page no-sidebar menu-has-search header-style-1">



<div id="wrapper" class="animsition">

<div id="page" class="clearfix">

    <!-- Header Wrap -->

    <div id="site-header-wrap">

        <!-- Top Bar -->        

        <div id="top-bar">

            <div id="top-bar-inner" class="wprt-container">

                <div class="top-bar-inner-wrap">

                    <div class="top-bar-content style-1">

                        <span class=" content">
						<img alt="" src="assets/img/tel.png" style="margin-right: 10px" > 31 2571-5995 / 2571-5997</span>

                        <span class=" content"><img alt="" src="assets/img/local.png" style="margin-right: 10px" > Av Das Américas, 354 -  B. Filadélfia</span>

                    </div><!-- /.top-bar-content -->



                    <div class="top-bar-socials style-1">

                        <div class="inner">

                            <span class="icons">

                                
                                <a href="#" title="Facebook">

                                    <span class="fa" aria-hidden="true"><img alt="" src="assets/img/face.png" ></span>

                                </a>



                                <a href="#" title="Instagram"><span class="fa" aria-hidden="true"><img alt="" src="assets/img/insta.png" ></span></a></span></div>

                    </div><!-- /.top-bar-socials -->

                </div>

            </div>

        </div>

        <!-- /Top Bar -->



        <!-- Header -->

        <header id="site-header">

            <div id="site-header-inner" class="wprt-container">

                <div class="site-header-wrap-inner">

                    <div id="site-logo" class="clearfix">

                        <div id="site-logo-inner">

                            <a href="index.php" title="Uniforbet" rel="home" class="main-logo">

                                <img src="assets/img/logo.png" width="210" height="42" alt="Contrucst" data-retina="assets/img/logo@2x.png" data-width="210" data-height="42">

                            </a>

                        </div>

                    </div><!-- /#site-logo -->



                    <div class="mobile-button"><span></span></div><!-- /mobile menu button -->



                    <nav id="main-nav" class="main-nav">

                        <ul>
                        
                        <li class="current-menu-item"><a href="#">Home</a></li>

                        <li><a href="empresa">A empresa</a></li>

                        <li><a href="uniformes">Uniformes</a></li>
                        
                        <li><a href="clientes">Clientes</a></li>

                        <li><a href="tecidos">Tecidos</a></li>
                        
                        <li><a href="bordados">Bordados</a></li>
                        
                        <li><a href="orcamento">Orçamento</a></li>

                        <li><a href="contato">Contato</a></li>

                        </ul>

                    </nav><!-- /#main-nav -->



                    <!-- /#header-search -->

                </div>

            </div>

        </header>

        <!-- /Header -->

    </div>

    <!-- /Header Wrap -->



    <!-- Main Content -->

    <div id="main-content" class="site-main clearfix">

        <div id="content-wrap">

            <div id="site-content" class="site-content clearfix">

                <div id="inner-content" class="inner-content-wrap">

                    <div class="page-content">


                        <div id="featured-title" class="clearfix featured-title-left">
						        <div id="featured-title-inner" class="wprt-container clearfix">
						            <div class="featured-title-inner-wrap">
						                <div class="featured-title-heading-wrap">
						                    <h1 class="featured-title-heading ">A EMPRESA</h1>
						                </div>
						                
						                
						            </div>
						        </div>
						    </div>




                        <!-- Testimonials 1 -->

                        <div class="row-testimonials-1">

                            <div class="container">

                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="wprt-spacer clearfix" data-desktop="74" data-mobi="60" data-smobi="40"></div>



                                        <div class="wprt-headings text-center" data-font="54" data-mfont="40">

                                            <h3 class="heading">DEPOIMENTOS</h3>



                                            <div class="sep"></div>



                                            <p class="sub-heading">

                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 

                                                Nunc pulvinar cursus massa. Nullam leo ex, interdum

                                            </p>

                                        </div>



                                        <div class="wprt-spacer clearfix" data-desktop="57" data-mobi="40" data-smobi="40"></div>



                                        <div class="wprt-carousel-box bullet-style-2 bullet-circle offset0 offset-v0 has-bullets" data-auto="false" data-loop="false" data-gap="30" data-column="2" data-column2="1" data-column3="1">

                                            <div class="owl-carousel owl-theme">

                                                <div class="wprt-testimonials clearfix style-3 testimonials-1">

                                                    <div class="item">

                                                        <div class="inner">

                                                            <blockquote class="text">

                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pulvin 

                                                                cursus massa. Nullam leo ex, interdum vitae imperdiet et, viverra 

                                                                ultricies magna. Vivamus sed luctus odio. Aenean a blandit eros. 

                                                                Praesent dignissim dapibus lacus, nec pellentesque nibh. Integer at 

                                                                convallis erat. Aenean aliquam interdum metus,

                                                                <div class="arrow"></div>

                                                                <div class="arrow2"></div>

                                                            </blockquote>



                                                            <div class="person">

                                                                <div class="avatar">

                                                                    <div class="thumb">

                                                                        <img src="assets/img/client-1.png" alt="image">

                                                                    </div>

                                                                </div>



                                                                <div class="info">

                                                                    <h6 class="name">DAVID MULLER</h6>

                                                                    <span>Designer</span>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div><!-- /.item -->

                                                </div><!-- /.wprt-testimonials -->



                                                <div class="wprt-testimonials clearfix style-3 testimonials-1">

                                                    <div class="item">

                                                        <div class="inner">

                                                            <blockquote class="text">

                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pulvin 

                                                                cursus massa. Nullam leo ex, interdum vitae imperdiet et, viverra 

                                                                ultricies magna. Vivamus sed luctus odio. Aenean a blandit eros. 

                                                                Praesent dignissim dapibus lacus, nec pellentesque nibh. Integer at 

                                                                convallis erat. Aenean aliquam interdum metus,

                                                                <div class="arrow"></div>

                                                                <div class="arrow2"></div>

                                                            </blockquote>



                                                            <div class="person">

                                                                <div class="avatar">

                                                                    <div class="thumb">

                                                                        <img src="assets/img/client-2.png" alt="image">

                                                                    </div>

                                                                </div>



                                                                <div class="info">

                                                                    <h6 class="name">DAVID MULLER</h6>

                                                                    <span>Designer</span>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div><!-- /.item -->

                                                </div><!-- /.wprt-testimonials -->



                                                <div class="wprt-testimonials clearfix style-3 testimonials-1">

                                                    <div class="item">

                                                        <div class="inner">

                                                            <blockquote class="text">

                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pulvin 

                                                                cursus massa. Nullam leo ex, interdum vitae imperdiet et, viverra 

                                                                ultricies magna. Vivamus sed luctus odio. Aenean a blandit eros. 

                                                                Praesent dignissim dapibus lacus, nec pellentesque nibh. Integer at 

                                                                convallis erat. Aenean aliquam interdum metus,

                                                                <div class="arrow"></div>

                                                                <div class="arrow2"></div>

                                                            </blockquote>



                                                            <div class="person">

                                                                <div class="avatar">

                                                                    <div class="thumb">

                                                                        <img src="assets/img/client-1.png" alt="image">

                                                                    </div>

                                                                </div>



                                                                <div class="info">

                                                                    <h6 class="name">DAVID MULLER</h6>

                                                                    <span>Designer</span>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div><!-- /.item -->

                                                </div><!-- /.wprt-testimonials -->



                                                <div class="wprt-testimonials clearfix style-3 testimonials-1">

                                                    <div class="item">

                                                        <div class="inner">

                                                            <blockquote class="text">

                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pulvin 

                                                                cursus massa. Nullam leo ex, interdum vitae imperdiet et, viverra 

                                                                ultricies magna. Vivamus sed luctus odio. Aenean a blandit eros. 

                                                                Praesent dignissim dapibus lacus, nec pellentesque nibh. Integer at 

                                                                convallis erat. Aenean aliquam interdum metus,

                                                                <div class="arrow"></div>

                                                                <div class="arrow2"></div>

                                                            </blockquote>



                                                            <div class="person">

                                                                <div class="avatar">

                                                                    <div class="thumb">

                                                                        <img src="assets/img/client-2.png" alt="image">

                                                                    </div>

                                                                </div>



                                                                <div class="info">

                                                                    <h6 class="name">DAVID MULLER</h6>

                                                                    <span>Designer</span>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div><!-- /.item -->

                                                </div><!-- /.wprt-testimonials -->



                                                <div class="wprt-testimonials clearfix style-3 testimonials-1">

                                                    <div class="item">

                                                        <div class="inner">

                                                            <blockquote class="text">

                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pulvin 

                                                                cursus massa. Nullam leo ex, interdum vitae imperdiet et, viverra 

                                                                ultricies magna. Vivamus sed luctus odio. Aenean a blandit eros. 

                                                                Praesent dignissim dapibus lacus, nec pellentesque nibh. Integer at 

                                                                convallis erat. Aenean aliquam interdum metus,

                                                                <div class="arrow"></div>

                                                                <div class="arrow2"></div>

                                                            </blockquote>



                                                            <div class="person">

                                                                <div class="avatar">

                                                                    <div class="thumb">

                                                                        <img src="assets/img/client-1.png" alt="image">

                                                                    </div>

                                                                </div>



                                                                <div class="info">

                                                                    <h6 class="name">DAVID MULLER</h6>

                                                                    <span>Designer</span>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div><!-- /.item -->

                                                </div><!-- /.wprt-testimonials -->



                                                <div class="wprt-testimonials clearfix style-3 testimonials-1">

                                                    <div class="item">

                                                        <div class="inner">

                                                            <blockquote class="text">

                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pulvin 

                                                                cursus massa. Nullam leo ex, interdum vitae imperdiet et, viverra 

                                                                ultricies magna. Vivamus sed luctus odio. Aenean a blandit eros. 

                                                                Praesent dignissim dapibus lacus, nec pellentesque nibh. Integer at 

                                                                convallis erat. Aenean aliquam interdum metus,

                                                                <div class="arrow"></div>

                                                                <div class="arrow2"></div>

                                                            </blockquote>



                                                            <div class="person">

                                                                <div class="avatar">

                                                                    <div class="thumb">

                                                                        <img src="assets/img/client-2.png" alt="image">

                                                                    </div>

                                                                </div>



                                                                <div class="info">

                                                                    <h6 class="name">DAVID MULLER</h6>

                                                                    <span>Designer</span>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div><!-- /.item -->

                                                </div><!-- /.wprt-testimonials -->

                                                

                                            </div>

                                        </div><!-- /.wprt-carousel-box -->



                                        <div class="wprt-spacer clearfix" data-desktop="80" data-mobi="60" data-smobi="40"></div>

                                    </div><!-- /.col-md-12 -->

                                </div><!-- /.row -->

                            </div><!-- /.container -->

                        </div>

                        <!-- /Testimonials 1 -->

                    </div><!-- /.page-content -->

                </div><!-- /#inner-content -->

            </div><!-- /#site-content -->

        </div><!-- /#content-wrap -->

    </div>

    <!-- /Main Content -->



    <!-- Footer -->

    <footer id="footer">

        <div id="footer-widgets" class="wprt-container">

            <div class="wprt-row gutter-30">

                <div class="col span_1_of_1" style="text-align: center">

                    <div class="widget widget-about">

                        
                        <div style="padding-bottom: 30px">
							<img alt="" src="assets/img/logo_rdp.png" style="height: 65px" width="210"></div>



                        <div class="text">

                            <p>Faça seu uniforme com a gente! O uniforme é o cartão de visita de uma empresa. Entre em contato e faça um orçamento sem compromisso.<br>
							<br>
							<img alt="" src="assets/img/telrdp.png" style="height: 26px" width="137"></p>

                        </div>

                    </div><!-- /.widget-about -->



                    <div class="widget widget-socials">


                        <a href="#"><span class="fa " aria-hidden="true"><img alt="" src="assets/img/face.png" ></span></a>

                        <a href="#"><span class="fa " aria-hidden="true"><img alt="" src="assets/img/insta.png" ></span></a>


                    </div><!-- /.widget-socials -->

                </div>



                



                



                

            </div><!-- /.wprt-row -->

        </div><!-- /#footer-widgets -->

    </footer>

    <!-- /Footer -->



    <!-- Bottom -->

    <div id="bottom" class="clearfix">

        <div id="bottom-bar-inner" class="wprt-container">

            <div class="bottom-bar-inner-wrap">

                <p class="copyright">2018 Uniforbet Uniformes Profissionais - All rights reserved.</p> 

            </div>

        </div>

    </div>

    <!-- /Bottom -->

</div><!-- /#page -->

</div><!-- /#wrapper -->



<a id="scroll-top"><img alt="" src="assets/img/subir.png" ></a>

    

<!-- Java Script -->

<script type="text/javascript" src="assets/js/jquery.min.js"></script>

<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

<script type="text/javascript" src="assets/js/animsition.js"></script>

<script type="text/javascript" src="assets/js/waypoint.js"></script>

<script type="text/javascript" src="assets/js/parallax.js"></script>

<script type="text/javascript" src="assets/js/matchMedia.js"></script>

<script type="text/javascript" src="assets/js/fitvids.js"></script>

<script type="text/javascript" src="assets/js/easing.js"></script>

<script type="text/javascript" src="assets/js/countTo.js"></script>

<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>

<script type="text/javascript" src="assets/js/cubeportfolio.js"></script>

<script type="text/javascript" src="assets/js/magnific.popup.min.js"></script>

<script type="text/javascript" src="assets/js/shortcodes.js"></script>

<script type="text/javascript" src="assets/js/main.js"></script>



<!-- Revolution Slider -->

<script type="text/javascript" src="includes/rev-slider/js/jquery.themepunch.tools.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="assets/js/rev-slider.js"></script>

<!-- Load Extensions only on Local File Systems ! The following part can be removed on Server for On Demand Loading -->  

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.actions.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.carousel.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.kenburn.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.migration.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.navigation.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.parallax.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.slideanims.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.video.min.js"></script>



</body>


<!-- Mirrored from www.cvtsoft.com/template/construct/ by HTTrack Website Copier/3.x [XR&CO'2010], Wed, 21 Feb 2018 23:19:14 GMT -->
</html>