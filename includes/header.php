<!DOCTYPE html>

<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<!--<![endif]-->


<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-QN3D2XWC7S"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-QN3D2XWC7S');
    </script>

    <!-- Basic Page Needs -->

    <meta charset="utf-8">

    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <title><?php if (stripos($_SERVER['REQUEST_URI'], 'empresa')) {
                echo 'A Empresa';
            }elseif(stripos($_SERVER['REQUEST_URI'], 'uniformes')){
                echo 'Uniformes';
            }elseif(stripos($_SERVER['REQUEST_URI'], 'clientes')){
                echo 'Clientes';
            }elseif(stripos($_SERVER['REQUEST_URI'], 'tecidos')){
                echo 'Tecidos';
            }elseif(stripos($_SERVER['REQUEST_URI'], 'bordados')){
                echo 'Bordados';
            }elseif(stripos($_SERVER['REQUEST_URI'], 'orcamento')){
                echo 'Orçamento';
            }elseif(stripos($_SERVER['REQUEST_URI'], 'contato')){
                echo 'Contato';
            }
            ?> | Uniforbet Uniformes Profissionais e Camisetas em Betim</title>

    <meta name="description" content="O melhor em uniformes em Betim. Alta Tecnologia, Agilidade na Entrega e Altíssima Precisão em Todos Detalhes. Comodidades: Atendimento Direto, Agilidade na Entrega, Redução de Custos">

    <meta name="keywords" content="uniformes em betim, camisas em betim, camisetas em betim, bordados em betim, uniforme administrativo em betim, uniforme operacional em betim, tecidos em betim, camisas, camisetas, bordados, uniforme administrativo, uniforme operacional, tecidos">




    <!-- Mobile Specific Metas -->

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />

    <!-- Theme Style -->

    <link rel="stylesheet" type="text/css" href="style.min.css">



    <!-- Favicon and touch icons  -->

    <link rel="shortcut icon" href="assets/icon/favicon.png">

    <link rel="apple-touch-icon-precomposed" href="assets/icon/apple-touch-icon-158-precomposed.png">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

</head>

<script type="text/javascript">
    (function() {
        var options = {
            whatsapp: "+5531996207696", // Número do WhatsApp
            company_logo_url: "//www.uniforbet.com.br/log.png", // URL com o logo da empresa
            greeting_message: "Olá! Seja bem vindo ao whatsapp da Uniforbet Uniformes Profissionais", // Texto principal
            call_to_action: "Envie-nos uma mensagem", // Chamada para ação
            position: "left", // Posição do widget na página 'right' ou 'left'
        };
        var proto = document.location.protocol,
            host = "whatshelp.io",
            url = proto + "//static." + host;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = url + '/widget-send-button/js/init.js';
        s.onload = function() {
            WhWidgetSendButton.init(host, proto, options);
        };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
</script>

<body class="front-page no-sidebar menu-has-search header-style-1">



    <div id="wrapper" class="animsition">

        <div id="page" class="clearfix">

            <!-- Header Wrap -->

            <div id="site-header-wrap">

                <!-- Top Bar -->

                <div id="top-bar">

                    <div id="top-bar-inner" class="wprt-container">

                        <div class="top-bar-inner-wrap">

                            <div class="top-bar-content style-1">

                                <span class=" content">
                                    <img alt="" src="assets/img/tel.png" style="margin-right: 10px" /> <a target="_blank" href="tel:3125715995" />31 2571-5995</a> / <a target="_blank" href="tel:3125715997" />2571-5997</a></span>

                                <span class="content"><img alt="" src="assets/img/local.png" style="margin-right: 10px"> <a target="_blank" href="https://www.google.com/maps/place/UNIFORBET+UNIFORMES+PROFISSIONAIS/@-19.9682689,-44.1920461,15z/data=!4m2!3m1!1s0x0:0xb927590db136aa2c?sa=X&ved=2ahUKEwiUlc_L26ruAhUUDrkGHeKoDO8Q_BIwCnoECBcQBQ">Av Das Américas, 354 - B. Filadélfia</a></span>

                            </div><!-- /.top-bar-content -->



                            <div class="top-bar-socials style-1">

                                <div class="inner">

                                    <span class="icons">


                                        <a href="https://www.facebook.com/uniforbet/" target="_blank" title="Facebook">

                                            <span class="fa" aria-hidden="true"><img alt="" src="assets/img/face.png"></span>

                                        </a>



                                        <a href="https://www.instagram.com/uniforbet/" target="_blank" title="Instagram"><span class="fa" aria-hidden="true"><img alt="" src="assets/img/insta.png"></span></a></span>
                                </div>

                            </div><!-- /.top-bar-socials -->

                        </div>

                    </div>

                </div>

                <!-- /Top Bar -->



                <!-- Header -->

                <header id="site-header">

                    <div id="site-header-inner" class="wprt-container">

                        <div class="site-header-wrap-inner">

                            <div id="site-logo" class="clearfix">

                                <div id="site-logo-inner">

                                    <a href="index.php" title="Uniforbet - Uniformes em Betim" rel="home" class="main-logo">

                                        <img src="assets/img/logo.png" width="210" height="42" alt="Uniforbet - Uniformes em Betim" data-retina="assets/img/logo.png" data-width="210" data-height="42">

                                    </a>

                                </div>

                            </div><!-- /#site-logo -->



                            <div class="mobile-button"><span></span></div><!-- /mobile menu button -->



                            <nav id="main-nav" class="main-nav">

                                <ul>
                                    <?php $homepage =   '/'; ?>
                                    <li class="<?php
                                                $homepage = "/";
                                                $currentpage = $_SERVER['REQUEST_URI'];
                                                if ($homepage == $currentpage) {
                                                    echo "current-menu-item";
                                                }
                                                ?>">
                                        <a href="/">Home</a>
                                    </li>

                                    <li class="<?php if (stripos($_SERVER['REQUEST_URI'], 'empresa')) {
                                                    echo 'current-menu-item';
                                                } ?>">
                                        <a href="empresa">A empresa</a>
                                    </li>

                                    <li class="<?php if (stripos($_SERVER['REQUEST_URI'], 'uniformes')) {
                                                    echo 'current-menu-item';
                                                } ?>">
                                        <a href="uniformes">Uniformes</a>
                                    </li>

                                    <li class="<?php if (stripos($_SERVER['REQUEST_URI'], 'clientes')) {
                                                    echo 'current-menu-item';
                                                } ?>">
                                        <a href="clientes">Clientes</a>
                                    </li>

                                    <li class="<?php if (stripos($_SERVER['REQUEST_URI'], 'tecidos')) {
                                                    echo 'current-menu-item';
                                                } ?>">
                                        <a href="tecidos">Tecidos</a>
                                    </li>

                                    <li class="<?php if (stripos($_SERVER['REQUEST_URI'], 'bordados')) {
                                                    echo 'current-menu-item';
                                                } ?>">
                                        <a href="bordados">Bordados</a>
                                    </li>

                                    <li class="<?php if (stripos($_SERVER['REQUEST_URI'], 'orcamento')) {
                                                    echo 'current-menu-item';
                                                } ?>">
                                        <a href="orcamento">Orçamento</a>
                                    </li>

                                    <li class="<?php if (stripos($_SERVER['REQUEST_URI'], 'contato')) {
                                                    echo 'current-menu-item';
                                                } ?>">
                                        <a href="contato">Contato</a>
                                    </li>

                                </ul>

                            </nav><!-- /#main-nav -->



                            <!-- /#header-search -->

                        </div>

                    </div>

                </header>

                <!-- /Header -->

            </div>

            <!-- /Header Wrap -->