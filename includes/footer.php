<!-- Footer -->

<footer id="footer">

    <div id="footer-widgets" class="wprt-container">

        <div class="wprt-row gutter-30">

            <div class="col span_1_of_1" style="text-align: center">

                <div class="widget widget-about">


                    <div style="padding-bottom: 30px">
                        <a href="/">
                            <img alt="" src="assets/img/logo_rdp.png" style="height: 65px" width="210">
                        </a>
                    </div>



                    <div class="text">

                        <p>Faça seu uniforme com a gente! O uniforme é o cartão de visita de uma empresa. Entre em contato e faça um orçamento sem compromisso.<br>
                            <br>
                            <a href="tel:3125715995" target="_blank">
                                <img alt="" src="assets/img/telrdp.png" style="height: 26px" width="137">
                            </a>
                        </p>

                    </div>

                </div><!-- /.widget-about -->



                <div class="widget widget-socials">


                    <a href="https://www.facebook.com/uniforbet/" target="_blank"><span class="fa " aria-hidden="true"><img alt="" src="assets/img/face.png"></span></a>

                    <a href="https://www.instagram.com/uniforbet/" target="_blank"><span class="fa " aria-hidden="true"><img alt="" src="assets/img/insta.png"></span></a>


                </div><!-- /.widget-socials -->

            </div>













        </div><!-- /.wprt-row -->

    </div><!-- /#footer-widgets -->

</footer>

<!-- /Footer -->



<!-- Bottom -->

<div id="bottom" class="clearfix">

    <div id="bottom-bar-inner" class="wprt-container">

        <div class="bottom-bar-inner-wrap">

            <p class="copyright">2016 - 2021 Uniforbet Uniformes Profissionais - All rights reserved.</p>

        </div>

    </div>

</div>

<!-- /Bottom -->

</div><!-- /#page -->

</div><!-- /#wrapper -->



<a id="scroll-top"><img alt="" src="assets/img/subir.png"></a>



<!-- Java Script -->

<script type="text/javascript" src="assets/js/jquery.min.js"></script>

<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

<script type="text/javascript" src="assets/js/animsition.js"></script>

<script type="text/javascript" src="assets/js/waypoint.js"></script>

<script type="text/javascript" src="assets/js/parallax.js"></script>

<script type="text/javascript" src="assets/js/matchMedia.js"></script>

<script type="text/javascript" src="assets/js/fitvids.js"></script>

<script type="text/javascript" src="assets/js/easing.js"></script>

<script type="text/javascript" src="assets/js/countTo.js"></script>

<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>

<script type="text/javascript" src="assets/js/cubeportfolio.js"></script>

<script type="text/javascript" src="assets/js/magnific.popup.min.js"></script>

<script type="text/javascript" src="assets/js/shortcodes.js"></script>

<script type="text/javascript" src="assets/js/main.js"></script>



<!-- Revolution Slider -->

<script type="text/javascript" src="includes/rev-slider/js/jquery.themepunch.tools.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="assets/js/rev-slider.js"></script>

<!-- Load Extensions only on Local File Systems ! The following part can be removed on Server for On Demand Loading -->

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.actions.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.carousel.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.kenburn.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.migration.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.navigation.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.parallax.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.slideanims.min.js"></script>

<script type="text/javascript" src="includes/rev-slider/js/extensions/revolution.extension.video.min.js"></script>



</body>


<!-- Mirrored from www.cvtsoft.com/template/construct/ by HTTrack Website Copier/3.x [XR&CO'2010], Wed, 21 Feb 2018 23:19:14 GMT -->

</html>