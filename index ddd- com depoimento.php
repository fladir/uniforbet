<?php include 'includes/header.php' ?>




    <!-- Main Content -->

    <div id="main-content" class="site-main clearfix">

        <div id="content-wrap">

            <div id="site-content" class="site-content clearfix">

                <div id="inner-content" class="inner-content-wrap">

                    <div class="page-content">

                        <!-- Slider -->

                        <div class="rev_slider_wrapper fullwidthbanner-container">

                            <div id="rev-slider1" class="rev_slider fullwidthabanner">

                                <ul>

                                    <!-- Slide 1 -->

                                    <li data-transition="random">

                                        <!-- Main Image -->

                                        <img src="assets/img/slider/slide-3.jpg" alt="" data-bgposition="center center" data-no-retina>

                                       

                                        <!-- Layers -->

                                        <div class="tp-caption tp-resizeme text-white font-heading"

                                            data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"

                                            data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-100','-100']"

                                            data-fontsize="['48','48','48','40']"

                                            data-lineheight="['58','58','58','50']"

                                            data-width="full"

                                            data-height="none"

                                            data-whitespace="normal"

                                            data-transform_idle="o:1;"

                                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 

                                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 

                                            data-mask_in="x:0px;y:[100%];" 

                                            data-mask_out="x:inherit;y:inherit;" 

                                            data-start="700" 

                                            data-splitin="none" 

                                            data-splitout="none" 

                                            data-responsive_offset="on">

                                            CONHEÇA<br>NOSSA LINHA INDUSTRIAL

                                        </div>



                                        



                                        <div class="tp-caption"

                                            data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"

                                            data-y="['middle','middle','middle','middle']" data-voffset="['12','120','120','140']"

                                            data-width="full"

                                            data-height="none"

                                            data-whitespace="normal"

                                            data-transform_idle="o:1;"

                                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 

                                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 

                                            data-mask_in="x:0px;y:[100%];" 

                                            data-mask_out="x:inherit;y:inherit;" 

                                            data-start="1000" 

                                            data-splitin="none" 

                                            data-splitout="none" 

                                            data-responsive_offset="on">

                                            <a href="#" class="wprt-button big">CLIQUE AQUI</a>

                                        </div>

                                    </li>

                                    <!-- /End Slide 1 -->



                                    <!-- Slide 2 -->

                                    <li data-transition="random">

                                        <!-- Main Image -->

                                        <img src="assets/img/slider/slide-2.jpg" alt="" data-bgposition="center center" data-no-retina>

                                        

                                        <!-- Layers -->

                                        

                                        

                                        

                                        

                                        

                                    </li>
                                    
                                    
                                    <li data-transition="random">

                                        <!-- Main Image -->

                                        <img src="assets/img/slider/slide1.jpg" alt="" data-bgposition="center center" data-no-retina>

                                        

                                        <!-- Layers -->

                                        

                                        

                                        

                                        

                                        

                                    </li>
                                    

                                    <!-- /End Slide 2 -->

                                </ul>

                            </div>

                        </div>

                        <!-- /Slider -->

                        

                        <!-- Action Box 1 -->

                        

                        <!-- /Action Box 1 -->
                        
                        
                        <div id="main-content" class="site-main clearfix">
        <div id="content-wrap">
            <div id="site-content" class="site-content clearfix">
                <div id="inner-content" class="inner-content-wrap">
                    <div class="page-content">
                        <!-- Services 1 -->
                        <div class="row-services-1">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="wprt-spacer clearfix" data-desktop="80" data-mobi="60" data-smobi="40"></div>
                                        
                                        <div class="wprt-headings clearfix text-center">

                                                <h2 class="heading">Seja bem vindo ao nosso site</h2>

                                                <div class="sep"></div>

                                                <p class="sub-heading">

                                                    Nossa linha de produtos está apta a atender aos profissionais de diversos ramos de atividades que necessitam de uniformes: sociais, industriais, hospitalares, chefes de cozinha e equipe, limpeza e conservação, hotelaria, dentre outros, além de confeccionarmos uniformes personalizados para atender a necessidade especifica de cada cliente. 

                                                </p>

                                            </div>
                                            
                                            <div class="wprt-spacer clearfix" data-desktop="53" data-mobi="30" data-smobi="30"></div>
                                        
                                    </div>

                                    <div class="col-md-4">
                                        <div class="wprt-image-box style-1 clearfix ">
                                            <div class="item">
                                                <div class="inner">
                                                    <div class="thumb">
                                                        <img width="370" height="250" src="assets/img/services/service-1.jpg" alt="Image">
                                                    </div>
                                                    
                                                    <div class="text-wrap">
                                                        <h3 class="title"><a target="_blank" href="#">Quem Somos</a></h3>  

                                                        <div class="desc">A UNIFORGIS é uma empresa que atua no mercado produzindo uniformes profissionais com um maquinário altamente qualificado.</div> 

                                                        <div class="wprt-btn"><a target="_blank" class="simple-link font-heading" href="empresa">SAIBA MAIS</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- /.wprt-image-box style-1 -->

                                        

                                        <!-- /.wprt-image-box style-1 -->

                                        <div class="wprt-spacer clearfix" data-desktop="0" data-mobi="40" data-smobi="40"></div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="wprt-image-box style-1 clearfix ">
                                            <div class="item">
                                                <div class="inner">
                                                    <div class="thumb">
                                                        <img width="370" height="250" src="assets/img/services/service-2.jpg" alt="Image">
                                                    </div>
                                                    
                                                    <div class="text-wrap">
                                                        <h3 class="title"><a target="_blank" href="#">Uniformes</a></h3> 

                                                        <div class="desc">Completa linha de uniformes para diversos perfis profissionais. Ampla grade de tamanhos para garantir a perfeita adequação visual.</div> 

                                                        <div class="wprt-btn"><a target="_blank" class="simple-link font-heading" href="uniformes">SAIBA MAIS</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- /.wprt-image-box style-1 -->

                                        

                                        <!-- /.wprt-image-box style-1 -->

                                        <div class="wprt-spacer clearfix" data-desktop="0" data-mobi="40" data-smobi="40"></div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="wprt-image-box style-1 clearfix ">
                                            <div class="item">
                                                <div class="inner">
                                                    <div class="thumb">
                                                        <img width="370" height="250" src="assets/img/services/service-3.jpg" alt="Image">
                                                    </div>
                                                    
                                                    <div class="text-wrap">
                                                        <h3 class="title"><a target="_blank" href="#">Bordados</a> </h3>  

                                                        <div class="desc">Processo eletrônico onde bordamos sua logomarca em um software específico, de maneira que os fios utilizados formem o desenho desejado.</div> 

                                                        <div class="wprt-btn"><a target="_blank" class="simple-link font-heading" href="bordados">SAIBA MAIS</a> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- /.wprt-image-box style-1 -->

                                        

                                        <!-- /.wprt-image-box style-1 -->
                                    </div>

                                    <div class="col-md-12">
                                        <div class="wprt-spacer clearfix" data-desktop="80" data-mobi="60" data-smobi="40"></div>
                                    </div>
                                </div>
                            </div><!-- /.container -->
                        </div>

                    
                    </div><!-- /.page-content -->
                </div><!-- /#inner-content -->
            </div><!-- /#site-content -->
        </div><!-- /#content-wrap -->
    </div>

                    

                        <!-- Services 1 -->

                        

                        <!-- /Services 1 -->



                        <!-- Portfolio 1-->

                        

                        <!-- /Portfolio 1-->

                        

                        <!-- About 1-->

                        

                        <!-- /About 1 -->



                        <!-- Video 1 -->

                        

                        <!-- /Video 1 -->



                        <!-- News 1 -->

                        

                        <!-- /News 1 -->



                        <!-- Testimonials 1 -->

                        <div class="row-testimonials-1">

                            <div class="container">

                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="wprt-spacer clearfix" data-desktop="74" data-mobi="60" data-smobi="40"></div>



                                        <div class="wprt-headings text-center" data-font="54" data-mfont="40">

                                            <h3 class="heading">DEPOIMENTOS</h3>



                                            <div class="sep"></div>



                                            <p class="sub-heading">

                                                Veja abaixo alguns depoimentos de nossos clientes

                                            </p>

                                        </div>



                                        <div class="wprt-spacer clearfix" data-desktop="57" data-mobi="40" data-smobi="40"></div>



                                        <div class="wprt-carousel-box bullet-style-2 bullet-circle offset0 offset-v0 has-bullets" data-auto="false" data-loop="false" data-gap="30" data-column="2" data-column2="1" data-column3="1">

                                            <div class="owl-carousel owl-theme">

                                                <div class="wprt-testimonials clearfix style-3 testimonials-1">

                                                    <div class="item">

                                                        <div class="inner">

                                                            <blockquote class="text">

                                                                Quero agradecer a toda equipe da UNIFORBET, vocês têm um diferencial único do mercado atual, um atendimento perfeito, com uma qualidade em atender e superar nossas expectativas.
                                                                <div class="arrow"></div>

                                                                <div class="arrow2"></div>

                                                            </blockquote>



                                                            <div class="person">

                                                                <div class="avatar">

                                                                    <div class="thumb">

                                                                        <img src="assets/img/cliente1.png" alt="image">

                                                                    </div>

                                                                </div>



                                                                <div class="info">

                                                                    <h6 class="name">JUSSARA FONSECA</h6>

                                                                    <span>Forlan Planejados</span>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div><!-- /.item -->

                                                </div><!-- /.wprt-testimonials -->



                                                <div class="wprt-testimonials clearfix style-3 testimonials-1">

                                                    <div class="item">

                                                        <div class="inner">

                                                            <blockquote class="text">

                                                                Os uniformes novos fizeram toda diferença! Eu adorei a variedade de modelos e opções de tecidos, sem falar no atendimento diferenciado e satisfação de ver os funcionários bem vestidos!
                                                                <div class="arrow"></div>

                                                                <div class="arrow2"></div>

                                                            </blockquote>



                                                            <div class="person">

                                                                <div class="avatar">

                                                                    <div class="thumb">

                                                                        <img src="assets/img/cliente2.png" alt="image">

                                                                    </div>

                                                                </div>



                                                                <div class="info">

                                                                    <h6 class="name">REINALDO ROCHA</h6>

                                                                    <span>Britadora Santiago e Filhos</span>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div><!-- /.item -->

                                                </div><!-- /.wprt-testimonials -->



                                                <!-- /.wprt-testimonials -->



                                                <!-- /.wprt-testimonials -->



                                                <div class="wprt-testimonials clearfix style-3 testimonials-1">

                                                    <div class="item">

                                                        <div class="inner">

                                                            <blockquote class="text">

                                                                Informamos que a empresa cumpriu satisfatoriamente com todas as obrigações previstas, inclusive no que se refere ao prazo de entrega e qualidade dos serviços.
                                                                <div class="arrow"></div>

                                                                <div class="arrow2"></div>

                                                            </blockquote>



                                                            <div class="person">

                                                                <div class="avatar">

                                                                    <div class="thumb">

                                                                        <img src="assets/img/cliente3.png" alt="image">

                                                                    </div>

                                                                </div>



                                                                <div class="info">

                                                                    <h6 class="name">VÍTOR SENDIM</h6>

                                                                    <span>Biólogo</span>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div><!-- /.item -->

                                                </div><!-- /.wprt-testimonials -->



                                                <div class="wprt-testimonials clearfix style-3 testimonials-1">

                                                    <div class="item">

                                                        <div class="inner">

                                                            <blockquote class="text">

                                                                Ficamos muito satisfeitos com os mesmo e isso nos faz fortalecer ainda mais nossa parceria.
                                                                <div class="arrow"></div>

                                                                <div class="arrow2"></div>

                                                            </blockquote>



                                                            <div class="person">

                                                                <div class="avatar">

                                                                    <div class="thumb">

                                                                        <img src="assets/img/cliente4.png" alt="image">

                                                                    </div>

                                                                </div>



                                                                <div class="info">

                                                                    <h6 class="name">CRISTIANE EUGÊNIO</h6>

                                                                    <span>Pre Moldados Sampaio</span>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div><!-- /.item -->

                                                </div><!-- /.wprt-testimonials -->

                                                

                                            </div>

                                        </div><!-- /.wprt-carousel-box -->



                                        <div class="wprt-spacer clearfix" data-desktop="80" data-mobi="60" data-smobi="40"></div>

                                    </div><!-- /.col-md-12 -->

                                </div><!-- /.row -->

                            </div><!-- /.container -->

                        </div>

                        <!-- /Testimonials 1 -->

                    </div><!-- /.page-content -->

                </div><!-- /#inner-content -->

            </div><!-- /#site-content -->

        </div><!-- /#content-wrap -->

    </div>

    <!-- /Main Content -->



    <?php include 'includes/footer.php' ?>
